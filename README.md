# Setup Autopatch on Linux

> **Push out a cron job to autopatch and boot a RHEL-box if it's required.**  
> **You can adjust the time it's run as you want.**  
> Not necessarily meant as an Enterprise-level solution, as it's bit "hacky", but works nicely as long as you've got some monitoring set up.  


## Run like this:
`$ ansible-playbook enable_autopatch.yml`  

---

## IMPORTANT!  
Cron options are set in the `./group_vars/all.yml` file.  
However, host specific adjustments are possible by creating a file under `./host_vars/<fqdn>.yml`.  
It's recommended to copy one of these files, and change the name of the copied file.  
Cron options set in a *host_vars*-file will apply to **that host only**.  

Example directory as follows:  
`.`  
`├── group_vars`  
`│   └── all.yml`  
`└── host_vars`  
`      ├── node1.yml`  
`      └── <fqdn>.yml`  
  
It's also possible to include groups of hosts to run the cronjob at the same time, by setting up groups in the inventory and refering to those under ./group_vars, but you get the idea...   
## Modifiable options:

Once a ./host_vars/\[fqdn\].yml file is copied and renamed, you should consider changing the following options:  
1. c_name
2. c_state
3. c_weekday
4. c_minute
5. c_hour
6. c_day  

More about that below.  

---
On the targets, crontab will look something like this:
```bash
#Ansible: Autopatch | Set run/no_run flag
0 1 01 * * /usr/bin/echo '0' > /root/autopatch/autopatch_flagfile.txt
#Ansible: Autopatch | Run if the flagfile contains '0' during mon-thu between 23. and 31. each month
0 4 24-31 * * [ $(</root/autopatch/autopatch_flagfile.txt) -eq "0" ] && [ $(date +\%u) -le 4 ] && { printf "\%s: \n" "$(date "+\%F \%T")"; /root/autopatch/yum_run.sh ; } >> /var/log/autopatch_logfile.log 2>&1 ; [ $? -eq 0 ] && { printf "\%s: \n" "$(date "+\%F \%T")"; /root/autopatch/reboot_check.sh ; } >> /var/log/autopatch_logfile.log 2>&1

```

---

# BREAKDOWN OF THE ELEMENTS  

> Two crontabs needs to be set in order for this to work  
> 1. Write a '0' into a file on the first day of the month.
> 2. Check if the file contains a '0', then go on to set up autopatching.  


### Crontab 1:  

A very simple echo command writes a '0' into the file /root/autopatch/autopatch_flagfile.txt  
Should normally be run on the first day of the month, unless you want autopatch to run on this particular day.  


### Crontab 2:  

A not so simple crontab that contains a series of commands.  
Logical checks and 'yum' commands can be modified in the file "./roles/patch/vars/main.yml".  


##### Logical checks:

1. The "flagfile_check" 
2. The "workday_check"
3. The "exit_check"

If the file "autopatch_flagfile.txt" contains a '0' character, it will allow the cronjob to continue to the "workday_check".  
The latter will allow the cronjob to run only Mondays to Thursdays.  
The "exit_check" is used to check the exit status of the 'yum update' script.  

##### Scripts:

1. The "yum_run" script
2. The "reboot_check" script

 IF the cronjob is allowed to go on, based on the logical checks, the "yum_run" script does a *yum update*.  
 It also contains the "exit_check" on the yum command, and will write the error code to the log if it fails.  
 If the yum update went well, it will write a '1' character to "autopatch_flagfile.txt" file replacing the '0'.  
 The reason is to allow some flexibility in when the scripts are run, but we don't want it to run two or three days in a row.  
 Setting this "flag" to 1, will keep it from running until the first cronjob changes it back to '0' the next month.  

 The "exit_check" is also used to fire off the "reboot_check" script.  
 This script checks the current kernel version and compares it to what's available.  
 If there is a newer kernel version available, the server reboots to make use of the new kernel.  
 Information about what the script does, based on that check, is written to the log file.


##### The logic in short:

The cronjob follows this logic:  
1. Two logical checks
2. Runs the "yum update" script
3. Runs the "reboot" script
4. Logs the output to the log file  
> This can be found in the variable "c_job" in ./host_vars/\[fqdn\].yml and ./group_vars/all.yml


##### The log file:

On target hosts the log file can be found here: "/var/log/autopatch_logfile.log".  
This file should be monitored with, say Splunk or something, and a warning or incident triggered if it contains errors.

  

> Please note!  
> The character `%` needs to be escaped with a `\` in crontab.  
> The characters `\` and `"` needs to be escaped with a `\` in Ansible.  



### The other crontab options: 

Other variables in ./host_vars/\[fqdn\].yml and ./group_vars/all.yml are as follows:  
- *c_name*:
    - Inserts a comment into the crontab with the name and description of the job.
- *c_state*:
    - Sets the job to a "present" or "absent" state on the target host.
- *c_weekday*:
    - Day of the week that the job should run (0-6 for Sunday-Saturday, *, etc)
- *c_minute*:
    - Minute when the job should run (0-59, *, */2, etc)
- *c_hour*:
    - Hour when the job should run (0-23, *, */2, etc)
- *c_day*:
    - Day of the month the job should run (1-31, *, */2, etc)  

More information can be found in the official [Ansible documentation](https://docs.ansible.com/ansible/latest/modules/cron_module.html).  
